    <div class="row">  
        <table class="table table-striped table-condensed dataTable">
            <thead>
            
            <th class="col-md-1"></th>
            <th class="col-md-3">Artículo</th>
            <th class="col-md-1">Precio</th>
            <th class="col-md-1">Categoría</th>
            <th class="col-md-1">Acciones</th>
            
            </thead>
            <tbody>
                <?php foreach ($resultado as $articulo): ?>
                    <tr>
                        <td>
                            <img width="40px" src="<?php echo base_url('assets/images/articles/' . $articulo->codigo . '.jpg'); ?>" alt="<?php echo $articulo->nombre; ?>"> 
                        </td>
                        <td>
                            <?php echo $articulo->nombre; ?>
                        </td>
                        <td>
                            <?php echo $articulo->precio; ?>
                        </td>
                        <td>
                            <?php echo $articulo->categoria; ?>
                        </td>
                        <td>
                            <a href="<?php echo site_url('articulo/editar/' . $articulo->id); ?>" class="btn btn-sm btn-outline-info" title="Editar un registro">
                                <span class="fas fa-edit"></span>                                   
                            </a>
                        </td>

                        <td>
                            <a href="<?php echo site_url('articulo/borra/' . $articulo->id); ?>" class="btn btn-sm btn-outline-danger"onclick="return confirm('¿Estás seguro que deseas borrar el artículo?')" title="Borra un registro">
                                <span class="fas fa-trash"></span>                                   
                            </a>
                            <a href="<?php echo site_url('articulo/compra/' . $articulo->id); ?>" class="btn btn-sm btn-outline-info" title="Añade al carrito">
                                <span class="fas fa-cart-plus"></span>                                   
                            </a>

                        </td>

                    </tr>
                <?php endforeach; ?>
            </tbody>    
        </table>
    </div>    
</div> 
    </body>
</html>