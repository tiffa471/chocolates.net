<html>
    <head>
        <meta charset="UTF-8">
        <title>Mi primera Vista</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link href="<?php echo base_url('assets/fontawesome/css/all.css'); ?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"/>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/jQuery-3.3.1/jquery-3.3.1.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/DataTables-1.10.20/js/jquery.dataTables.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/DataTables-1.10.20/js/dataTables.bootstrap4.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/tienda.js'); ?>"></script>
        <script src="<?php echo base_url('assets/jQuery-3.3.1/jquery-3.3.1.js');?>" type="text/javascript"></script>

    </head>
    <body>
        <div class="container">
            <div class="row bg-danger">
                <div class="col-8">
                    <h3 class="text-white  col-12">
                        <?php echo $titulo; ?>
                    </h3>
                </div>
                <div class="col-1">
                    <h4>
                        <a href="" id="muestra-carro" data-toggle="modal" data-target="#carroModal">
                            <span class="fas fa-shopping-cart text-white"></span>
                        </a>
                    </h4>    
                </div>
                <div class="col-1">    
                    <span class="text-white">
                        <h3><?php echo isset($this->session->carrito) ? count($this->session->carrito) : 0 ; ?></h3>
                    </span>
                </div>
                <div class="col-2">
                    <span class="float-right">
                        <small class="text-white"><?php echo $this->ion_auth->user()->row()->last_name,', '.$this->ion_auth->user()->row()->first_name; ?></small>
                        <a href=<?php echo site_url('auth/logout/')?> >
                             <span class="fas fa-sign-out-alt" aria-hidden="true"></span>
                        </a>
                    </span> 
                </div>
            </div>  
        