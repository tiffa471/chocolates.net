        <div class="container">
                                   
            
            <?php echo form_open_multipart(site_url('articulo/formulario'),['class' => 'form-horizontal']);?>
            <div class="row">
                <div class="col-9">
                <div class="form-row">
                    <div class="col-md-3">
                <?php echo form_label('Código: ','codigo',['class' => 'control-label']);?>
                <?php $marca = form_error('codigo')!==''?'border-danger bg-warning':'';?>        
                <?php echo form_input(['name'=>'codigo', 'id'=>'codigo', 'class' => "form-control $marca", 'placeholder' => 'Código artículo', 'value'=> set_value('codigo')]);?>
                <?php echo form_error('codigo','<div class="small text-danger">','<div>') ;?>   
                    </div>
                    <div class="col-md-9">
                <?php echo form_label('Nombre: ','nombre',['class' => 'control-label']);?>
                <?php echo form_input(['name'=>'nombre', 'id'=>'nombre', 'class' => 'form-control', 'placeholder'=>'Introduce nombre artículo']);?>
                    </div>  
                </div>
            <div class="form-row">
                <div class="col-md-3">
                    <?php echo form_label('Capacidad: ','volumen',['class' => 'control-label']);?>
                    <?php echo form_input(['name'=>'volumen', 'id'=>'volumen', 'class' => 'form-control', 'placeholder'=>'Volumen artículo']);?>
                </div>
                <div class="col-md-3">
                    <?php echo form_label('Grados: ','grados',['class' => 'control-label']);?>
                    <?php echo form_input(['name'=>'grados', 'id'=>'grados', 'class' => 'form-control', 'placeholder'=>'Graduación']);?>
                </div>  
                <div class="col-md-3">
                    <?php echo form_label('Categoría: ','categoria',['class' => 'control-label']);?>
                    <!--<?php echo form_input(['name'=>'categoria', 'id'=>'categoria', 'class' => 'form-control', 'placeholder'=>'Categoría']);?>-->
                    <?php echo form_dropdown('categoria',$categorias, set_value('categoria'),['id'=>'categoria','class'=>'form-control']);?>

                    
                </div>
                <div class="col-md-3">
                    <?php echo form_label('Precio: ','precio',['class' => 'control-label']);?>
                    <?php echo form_input(['name'=>'precio', 'id'=>'precio', 'class' => 'form-control', 'placeholder'=>'Precio']);?>
                </div>  
            </div>
            <div class="form-row">
                <div class="col-md-12">
                    <?php echo form_label('Descripción: ','descripcion',['class' => 'control-label']);?>
                    <?php echo form_input(['name'=>'descripcion', 'id'=>'descripcion', 'class' => 'form-control', 'placeholder'=>'Introduzca una descripción']);?>
                </div>
            </div>
                </div>
                <div class="col-3">
                    <div class="col-12">
                    <div class="custom-file">
                     <?php echo form_upload(['name'=> 'imagen','id'=> 'imagen','class'=>'custom-file-control']);?>
                     <?php echo form_label('Selecciona la imagen jpg','imagen',['class'=>'custom-file-label']);?>   
                    </div>  
                    </div>
                </div>
            </div>
            <div class="form-row">
            
                <div class="col-md-12">
                    <?php echo form_submit('Nuevo','Enviar',['class'=>'btn btn-primary']);?>
                </div>
            </div>
            <?php echo form_close();?>
        </div>
        </body>
</html>