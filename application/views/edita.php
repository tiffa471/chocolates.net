        <div class="container">
                                   
            
            <?php echo form_open(site_url('edita'.$id),['class' => 'form-horizontal']);?>
            <div class="row">
                <div class="col-9">
                    <div class="form-row">
                        <div class="col-md-3">
                            <?php echo form_label('Código: ', 'codigo', ['class' => 'control-label']); ?>
                            <?php $marca = form_error('codigo') !== '' ? 'border-danger bg-warning' : ''; ?>        
                            <?php echo form_input(['name' => 'codigo', 'id' => 'codigo', 'class' => "form-control $marca", 'placeholder' => 'Código artículo', 'value' => set_value('codigo', $articulo->codigo)]); ?>
                            <?php echo form_error('codigo', '<div class="small text-danger">', '<div>'); ?>   
                        </div>
                        <div class="col-md-9">
                            <?php echo form_label('Nombre: ', 'nombre', ['class' => 'control-label']); ?>
                            <?php echo form_input(['name' => 'nombre', 'id' => 'nombre', 'class' => 'form-control', 'placeholder' => 'Introduce nombre artículo', 'value' => set_value('nombre', $articulo->nombre)]); ?>
                        </div>  
                    </div>
                    <div class="form-row">
                <div class="col-md-3">
                    <?php echo form_label('Categoría: ', 'categoria', ['class' => 'control-label']); ?>

                    <?php echo form_dropdown('categoria', $categorias, set_value('categoria'), ['id' => 'categoria', 'class' => 'form-control', 'value' => set_value('categoria', $articulo->categoria)]); ?>


                </div>
                <div class="col-md-3">
                    <?php echo form_label('Precio: ', 'precio', ['class' => 'control-label']); ?>
                    <?php echo form_input(['name' => 'precio', 'id' => 'precio', 'class' => 'form-control', 'placeholder' => 'Precio', 'value' => set_value('precio', $articulo->precio)]); ?>
                </div>  
            </div>
            <div class="form-row">
                <div class="col-md-12">
                    <?php echo form_label('Descripción: ', 'descripcion', ['class' => 'control-label']); ?>
                    <?php echo form_input(['name' => 'descripcion', 'id' => 'descripcion', 'class' => 'form-control', 'placeholder' => 'Introduzca una descripción', 'value' => set_value('descripcion', $articulo->descripcion)]); ?>
                </div>
            </div>
                </div>
            </div>
            <div class="form-row">

                <div class="col-md-12">
                    <?php echo form_submit('Nuevo', 'Enviar', ['class' => 'btn btn-primary']); ?>
                </div>
            </div>
            <?php echo form_close();?>
        </div>
        </body>
</html>