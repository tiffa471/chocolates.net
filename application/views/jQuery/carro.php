<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <title>jQuery: Ejemplo selector adyacente</title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/fontawesome/css/all.css');?>" />
        <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>"/>
        <script src="<?php echo base_url('assets/js/carro.js');?>" type="text/javascript"></script>
        <link href="<?php echo base_url('assets/css/carro.css');?>" rel="stylesheet" type="text/css"/>
        <script src="<?php echo base_url('assets/jQuery-3.3.1/jquery-3.3.1.js');?>" type="text/javascript"></script>
        <script type="text/javascript">
            $(function()
            {
                $(".item").css("background-color","#cecece");
                $("#cart_items").css("border","4px solid black");
                $("img").css("border","1px solid blue");
                $(".item > label").css("text-decoration","underline");
                $("#cart_container button").css("color","red");
                $("label+label").css("color","white");
                $("label:contains(€),input").css("color","green");
                $("div:empty").css("background-color","yellow");
                $(".item:first-child").css("background-color","red");
                $("#i5").css("background-color","red");
                $("#i1 > img").css("border-color","green");
                $("#i3 > img").css("border-color","green");
            });
        </script> 
    </head>
    <body>
        <div id="item_container">
		<div class="item" id="i1">
			<img src="<?php echo base_url('assets/images/carro/camiseta1.jpg');?>" alt="descripción i1"/>
			<label class="title">Camiseta 1</label>
			<label class="price">20 €</label>
			<label class="stock">Stock 10</label>
		</div>
		<div class="item" id="i2">
			<img src="<?php echo base_url('assets/images/carro/reloj2.jpg');?>" alt="descripción i2"/>
			<label class="title">Reloj 2</label>
			<label class="price">24 €</label>
			<label class="stock">Stock 10</label>
		</div>
		<div class="item" id="i3">
			<img src="<?php echo base_url('assets/images/carro/camiseta3.jpg');?>" alt="descripción i3"/>
			<label class="title">Camiseta 3</label>
			<label class="price">18 €</label>
			<label class="stock">Stock 10</label>
		</div>
		<div class="item" id="i4">
			<img src="<?php echo base_url('assets/images/carro/reloj4.jpg');?>" alt="descripción i4"/>
			<label class="title">Reloj 4</label>
			<label class="price">30 €</label>
			<label class="stock">Stock 10</label>
		</div>
		<div class="item" id="i5">
			<img src="<?php echo base_url('assets/images/carro/reloj5.jpg');?>" alt="descripción i5"/>
			<label class="title">Reloj 5</label>
			<label class="price">28 €</label>
			<label class="stock">Stock 10</label>
		</div>
		<div class="clear"></div>
	</div>
	<div id="cart_container">
		<div id="cart_title">
			<span>Carrito</span>
			<div class="clear"></div>
		</div>
		<div id="cart_toolbar">
			<div id="cart_items" class="back"></div>
		</div>
		<div id="navigate">
			<div id="nav_left">
				<button id="btn_comprar" class="btn btn-light" title="Confirma la compra de los artículos">Comprar</button>
				<button id="btn_prev" class="btn btn-light" title="Desplaza el carrito hacia la izquierda">&lt;</button>
				<button id="btn_next" class="btn btn-light" title="Desplaza el carrito hacia la derecha">&gt;</button>
				<button id="btn_clear" class="btn btn-light" title="Vacia el carrito">Vaciar</button>
			</div>
			<div id="nav_right">
				<span class="sptext">
					<label>Compras </label><input id="citem" value="0" readonly title="Número de productos comprados"/>
				</span>
				<span class="sptext">
					<label>Precio </label><input id="cprice" value="0 €" readonly  title="Precio total de los productos comprados"/>
				</span>
			</div>
			<div class="clear"></div>
		</div>
	</div>       
    </body>
</html>

