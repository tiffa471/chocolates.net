<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <title>jQuery</title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/fontawesome/css/all.css');?>" />
        <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>"/>
        <script src="<?php echo base_url('assets/jQuery-3.3.1/jquery-3.3.1.js');?>" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function()
            {
                $("p > span").css("color", "red");
            });
        </script>        
    </head>
    <body>
        <div class="container">
            <div>
                <h1>Bienvenidos a jQuery</h1>
            </div>
            <p>El texto <span>rojo</span> lo marco con span </p>
            <p><strong><span>Esto no es rojo</span></strong></p>
        </div>
    </body>
</html>