<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Mi primera Vista</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/jQuery-3.3.1/jquery-3.3.1.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/DataTables-1.10.20/js/jquery.dataTables.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/DataTables-1.10.20/js/dataTables.bootstrap4.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/tienda.js');?>"></script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <h1 class="text-white bg-danger col-12">
                    Lista de Artículos
                </h1>
            </div>    
            <div class="row">  
                <?php foreach ($resultado as $articulo): ?>
                <div class="col-sm-4">
                    <div class="card">
                        <img src="<?php echo base_url('assets/images/articles/'.$articulo->codigo.'.jpg');?>" class="card-img-top" alt="<?php echo $articulo->nombre; ?>">
                        <strong  class="text-center text-uppercase card-title bg-danger text-white"><?php echo $articulo->nombre; ?></strong>
                        <p class="card-body">
                            <?php echo substr($articulo->descripcion,0,100)." ..."; ?>
                             
                            <span style="color:blue"> ( <?php echo $articulo->volumen; ?> cl )</span>
                        </p>
                        <p class="text-right ">
                            <strong class="alert alert-info">
                                <?php echo $articulo->precio.' €'; ?>
                            </strong>
                        </p>    
                    </div>
                </div>
                <?php endforeach;?>
            </div>    
        </div>    
        <p>Holi</p>
    </body>    
</html>