<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tienda
 *
 * @author a023328480v
 */
class Tienda extends CI_Model{
    public function get_articulos() {
        $sql = <<< SQL
            SELECT * 
              FROM articulos
SQL;
        $consulta = $this->db->query($sql);
        return $consulta->result();
    }
    
    public function get_articulo($id) {
        $sql = <<< SQL
            SELECT * 
              FROM articulos
                WHERE id = ?
SQL;
        $consulta = $this->db->query($sql,[$id]);
        //solo el primero de un array de 1
        return $consulta->result()[0];
    }
    
    public function add_articulo($articulo){
        $this->db->insert('articulos',$articulo);
    }
    
    public function insertar($articulo){
        $this->db->insert('articulos',$articulo);
               
    }
    
    public function actualizar($articulo,$id){
        $this->db->set('articulos',$articulo,['id'=>$id]);
    }
    
    
    public function get_categorias(){
        $sql = <<< SQL
               SELECT *
                FROM categorias
SQL;
        $consulta = $this->db->query($sql);
        $array = [];
        foreach ($consulta->result() as $item){
            $array[$item->id] = $item->nombre;
            
        }
        return $array;
        
    }
    
    public function delete($id) {
        $this->db->delete('articulos', ['id' => $id]);
    }

}
