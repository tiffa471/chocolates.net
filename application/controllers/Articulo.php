<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Articulo
 *
 * @author a023328480v
 */
class Articulo extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->model('tienda');
        $this->load->library('session');
        $this->load->database();
	$this->load->library('ion_auth');
        $this->load->library('form_validation');
	$this->load->helper(['url', 'language']);
	$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
	$this->lang->load('auth');
        $this->load->library('ion_auth');
    }

    public function index() {
        $data['resultado'] = $this->tienda->get_articulos();
        $this->load->view('lista', $data);
    }

    public function tabla() {
        $data['resultado'] = $this->tienda->get_articulos();
        $data['titulo'] = 'Lista de artículos';
        $this->load->view('commons/header', $data);
        $this->load->view('tabla', $data);
    }

    public function formulario() {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('codigo', 'Código del Artículo', 'required|is_unique[articulos.codigo]|integer|trim');
        $this->form_validation->set_rules('nombre', 'Nombre del Artículo', 'required|trim');
        $this->form_validation->set_rules('volumen', 'Volumen del Artículo en cl', 'required|integer');
        $this->form_validation->set_rules('grado', 'Graduacion del Artículo en cl', 'required|integer');
        $this->form_validation->set_rules('precio', 'Precio del Artículo en cl', 'required|greater_than[2]|decimal');
        $this->form_validation->set_rules('categoria', 'Categoría del artículo', 'required');
        $this->form_validation->set_rules('descripcion', 'Descripción del artículo', 'required');
        if ($this->form_validation->run() === FALSE) {
            $data['titulo'] = 'Formulario introducción datos';
            //obtener datos de categorias
            $data['categorias'] = $this->tienda->get_categorias();
            $this->load->view('commons/header', $data);
            $this->load->view('formulario');
        } else {
            $articulo = [
                'codigo' => $this->input->post('codigo'),
                'nombre' => $this->input->post('nombre'),
                'volumen' => $this->input->post('volumen'),
                'grados' => $this->input->post('grados'),
                'categoria' => $this->input->post('categoria'),
                'precio' => $this->input->post('precio'),
                'descripcion' => $this->input->post('descripcion')
            ];
            $config['upload_path'] = 'assets/images/articles';
            $config['file_name'] = $articulo['codigo'] . '.jpg';
            $config['allowed_types'] = 'jpg';
            $config['max_size'] = 300; //kb
            $config['max_width'] = 1200;
            $config['max_height'] = 1600;
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('imagen')) {
                $this->tienda->insertar($articulo);
                redirect(site_url('articulos/formulario'));
                //la subida sea correcta    
            } else {
            //error de la subida    
                $data ['error'] = $this->upload->display_errors();
            }
            $this->load->view('exito');
        }
        $this->input->post('Articulo');
    }

    public function borra($id) {
        $this->tienda->delete($id);
        echo "<h1>El articulo ha sido borrado</h1>";
    }

    public function compra($id) {
        //carrito será la variable de sesion. array asociativo
        if ($this->session->has_userdata('carrito')) {
            //existe
            $carrito = $this->session->carrito;
            if (isset($carrito[$id])) {
                //ya tengo el articulo en el carrito
                $carrito[$id] ++;
            } else {
                //añadir el nuevo artículo
                $carrito[$id] = 1;
            }
        } else {
            //hay que crearla
            $carrito = [];
            $carrito[$id] = 1;
        }
        $this->session->set_userdata('carrito', $carrito);
        redirect(site_url('articulo/tabla'));
    }

    public function carrito() {
        echo "<pre>";
        print_r($this->session->carrito);
        echo "</pre>";
    }

    public function editar($id) {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('codigo', 'Código del Artículo', 'required|integer|trim');
        $this->form_validation->set_rules('nombre', 'Nombre del Artículo', 'required|trim');
        $this->form_validation->set_rules('volumen', 'Volumen del Artículo en cl', 'required|integer');
        $this->form_validation->set_rules('grado', 'Graduacion del Artículo en cl', 'required|integer');
        $this->form_validation->set_rules('precio', 'Precio del Artículo en cl', 'required|greater_than[2]|decimal');
        $this->form_validation->set_rules('categoria', 'Categoría del artículo', 'required');
        $this->form_validation->set_rules('descripcion', 'Descripción del artículo', 'required');
        if ($this->form_validation->run() === FALSE) {
            $data['titulo'] = 'Formulario edicion de articulo';
            //obtener datos de categorias
            $data['categorias'] = $this->tienda->get_categorias();
            //obtener los valores del articulo a modificar
            $data['articulo'] = $this->tienda->get_articulo($id);
            $data['id'] = $id;
            $this->load->view('commons/header', $data);
            $this->load->view('edita', $data);
        } else {
            $articulo = [
                'codigo' => $this->input->post('codigo'),
                'nombre' => $this->input->post('nombre'),
                'volumen' => $this->input->post('volumen'),
                'grados' => $this->input->post('grados'),
                'categoria' => $this->input->post('categoria'),
                'precio' => $this->input->post('precio'),
                'descripcion' => $this->input->post('descripcion')
            ];
            $this->tienda->actualizar($articulo, $id);
        }
    }

    public function pruebadbclick(){
    $this->load->view('dbclick');    
    $this->load->artículo;
    }
    
    public function dbclick(){
        //carrito será la variable de sesion. array asociativo
        if ($this->session->has_userdata('carrito')) {
            //existe
            $carrito = $this->session->carrito;
            if (isset($carrito[$id])) {
                //ya tengo el articulo en el carrito
                $carrito[$id] ++;
            } else {
                //añadir el nuevo artículo
                $carrito[$id] = 1;
            }
        } else {
            //hay que crearla
            $carrito = [];
            $carrito[$id] = 1;
        }
        $this->session->set_userdata('carrito', $carrito);
        redirect(site_url('articulo/tabla'));
    }
    
    
    
    
    
    
    
    
    
    
 
    
}
