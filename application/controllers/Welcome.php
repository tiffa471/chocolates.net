<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function index()
	{
		$this->load->view('welcome_message');
	}
        public function ausias()
        {
                $this->load->view('mensaje_ausias');
        }
        public function saludo($user)
        {
                $data['usuario'] = $user;
                $this->load->view('mensaje_ausias',$data);
        }
}
