<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Jquery extends CI_Controller{

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->model('tienda');
        $this->load->library('session');
    }
    
    public function index() {
        $this->load->view('jQuery/ejemplo');
    }
    
    public function carro() {
        $this->load->view('jQuery/carro');
    }
}
